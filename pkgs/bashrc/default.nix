{ stdenv }:

stdenv.mkDerivation rec {
  name = "bashrc";

  phases = [ "installPhase" ];

  src = ./.;

  installPhase = ''
    install -dm 755 $out/home
    cp $src/bashrc $out/home/.bashrc
  '';

}
